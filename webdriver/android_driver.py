#!/usr/bin/env python
# coding: utf-8

from appium import webdriver
import os


class Driver:

    def __init__(self):
        """ Comment platformName
                    platformVersion
                    deviceName
                    app
        if running on aws device farm
        """

        #apk = os.environ["APK"]
        #user = os.environ["USER"]

        desired_caps = {

            'platformName': 'android',
            'deviceName': 'Galaxy S9',
            'appPackage': 'co.respiri.weebie.qa',
            'appActivity': 'co.respiri.weebie.view.main.MainActivity',
            "app": "/tmp/Wheezo_Qa_659.apk",
            "newCommandTimeout": '7200'
        }


        self.instance = webdriver.Remote('http://127.0.0.1:4723/wd/hub',
                                         desired_caps)

        screenshotDir = '%s/' % os.getcwd()
        file_name = 'screenshot.png'

        self.instance.save_screenshot(screenshotDir + file_name)