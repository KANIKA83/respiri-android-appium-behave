#!/usr/bin/env python
# coding: utf-8

import time
import subprocess

from webdriver.android_driver import Driver
from appium.webdriver.common.mobileby import MobileBy
from appium.webdriver.common.touch_action import TouchAction
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import TimeoutException


class Utilities():

    @staticmethod
    def mobile_waitForElement(driver, elem_id, timeout=15):
        """
        Searches for the element with the specified accessibility ID. Timeouts
        after specified amount of time. Timeout is set to 10s.

        :param driver: The device that is being tested
        :param elem_id: The accessiblility ID of the element
        :para timeout: Time to wait before throwing error.
        """
        try:
            element = WebDriverWait(driver, timeout).until(
                EC.visibility_of_element_located((
                    MobileBy.ID, elem_id)))
            print("Waiting for element: {}.".format(elem_id))
            result_flag = True
        except NoSuchElementException:

            raise NoSuchElementException(
                "\nThe element {} does not"
                " exist on the current screen.".format(elem_id)

            )
        print("Found element: {}.".format(elem_id))
        return result_flag

    @staticmethod
    def wait_for_element_by_text(driver, text):
        timeout = time.time() + 10
        var = 'new UiSelector().text(' + '"' + text + '")'
        while time.time() < timeout:
            try:
                element = driver.find_element_by_android_uiautomator(var)
                if element.is_displayed():
                    print("Found text: {}".format(text))
                    return True
            except NoSuchElementException:
                print("Not found text yet: {}.".format(text))
        raise NoSuchElementException(
            "\nThe text {} does not"
            " exist on the current screen.".format(text)
        )

    @staticmethod
    def wait_for_element_by_class(driver, class_name):
        timeout = time.time() + 10
        while time.time() < timeout:
            try:
                element = driver.find_element_by_class_name(class_name)
                if element.is_displayed():
                    print("Found class: {}".format(class_name))
                    return True
            except NoSuchElementException:
                print("Not found class yet: {}.".format(class_name))
        raise NoSuchElementException(
            "\nThe class {} does not"
            " exist on the current screen.".format(class_name)
        )


    @staticmethod
    def assert_false(driver, elem_id, timeout=0):
        """
        Searches for the element with the specified accessibility ID and
        raises Exception if it exists, ignores TimeoutException.

        :param driver: The device that is being tested
        :param elem_id: The accessiblility ID of the element
        :para timeout: Time to wait before throwing error.
        """
        try:
            element = WebDriverWait(driver, timeout).until(
                EC.visibility_of_element_located((
                    MobileBy.ID, elem_id)))
            print(element)
            if element is not None:
                raise Exception(
                    "\nThe element should not"
                    "exist on the current screen."
                )
        except TimeoutException:
            print("\nThe element correctly does not"
                  "exist on the current screen.")
            pass

    @staticmethod
    def mobile_clickElementByID(driver, elem_id, timeout=10):
        """
        Searches for the element with the specified accessibility ID and then taps on it.
        Timeoutsafter specified amount of time. Timeout is set to 10s.

        :param driver: The device that is being tested
        :param elem_id: The accessiblility ID of the element
        :para timeout: Time to wait before throwing error.
        """
        try:
            element = WebDriverWait(driver, timeout).until(
                EC.visibility_of_element_located((
                    MobileBy.ID, elem_id)))
            element.click()
        except NoSuchElementException:
            raise NoSuchElementException(
                "\nThe element {} does not"
                " exist on the current screen.".format(elem_id)
            )
        print("Tapped element: {}.".format(elem_id))

    @staticmethod
    def click_element_by_id(driver, elem_id, timeout=10):
        try:
            element = driver.find_element_by_accessibility_id(elem_id)
            element.click()
        except NoSuchElementException:
            raise NoSuchElementException(
                "\nThe element {} does not"
                " exist on the current screen.".format(elem_id)
            )
        return element

    @staticmethod
    def click_element_by_name(driver, text, timeout=10):
        text = 'new UiSelector().text(' + '"' + text + '")'
        element = driver.find_element_by_android_uiautomator(text)
        element.click()

    @staticmethod
    def click_element_by_class(driver, class_name, timeout=10):
        try:
            element = driver.find_element_by_class_name(class_name)
            element.click()
        except NoSuchElementException:
            raise NoSuchElementException(
                "\nThe class {} does not"
                " exist on the current screen.".format(class_name)
            )
        return element

    @staticmethod
    def assert_element(driver, elem_id, timeout=10):
        try:
            element = WebDriverWait(driver, timeout).until(
                EC.visibility_of_element_located((
                    MobileBy.ID, elem_id)))
            element.is_displayed()
        except:
            raise NoSuchElementException(
                "\nThe element {} does not"
                " exist on the current screen.".format(elem_id)
            )

    @staticmethod
    def assert_text(driver, elem_id, text, timeout=10):
        """
        Get element attribute text using the accessibility ID
        and matches it with text param.

        :param driver: The device that is being tested
        :param elem_id: The accessiblility ID of the element
        :param text: Text to match
        :para timeout: Time to wait before throwing error.
        """
        element = WebDriverWait(driver, timeout).until(
            EC.visibility_of_element_located((
                MobileBy.ID, elem_id)))
        # print("Asserting text {}") .format(text)
        assert text == element.get_attribute(
            'text'), "Text not found: {}".format(text)

    @staticmethod
    def assert_textContains(driver, elem_id, text, timeout=10):
        """
        Matches text that is a substring of an element using the
        accessbility ID.

        :param driver: The device that is being tested
        :param elem_id: The accessiblility ID of the element
        :param text: Substring of text to match
        :para timeout: Time to wait before throwing error.

        :return: element
        """
        element = WebDriverWait(driver, timeout).until(
            EC.visibility_of_element_located((
                MobileBy.ID, elem_id)))
        element = element.get_attribute('text')
        # print("Asserting text contains {}") .format(text)
        assert text in element, "Text does not contain: {}".format(text)

    @staticmethod
    def enter_text(driver, elem_id, text, timeout=10):
        """
        Enters text in an element using the accessibility ID.

        :param driver: The device that is being tested
        :param elem_id: The accessiblility ID of the element
        :param text: Text to sends keys
        :para timeout: Time to wait before throwing error.
        """
        element = WebDriverWait(driver, timeout).until(
            EC.visibility_of_element_located((
                MobileBy.ID, elem_id)))
        element.set_value(text)

    @staticmethod
    def enter_text_using_class(driver, class_name, text, timeout=10):
        """
        Enters text in an element using the class.

        :param driver: The device that is being tested
        :param class_name: The class name of the element
        :param index: index of the view
        :para timeout: Time to wait before throwing error.
        """
        element = driver.find_element_by_class_name(class_name)
        element.set_value(text)


    @staticmethod
    def adb_key(self, key):
        # cmd = "adb shell input keyevent KEYCODE_{}".format(key)
        p = subprocess.Popen(
            ["adb", "shell", "input", "keyevent", "KEYCODE_{}"],
            stdout=subprocess.PIPE).format(key)
        print("\nADB button")

    @staticmethod
    def press_keycode(driver, key):
        driver.press_keycode(key)

    @staticmethod
    def press_back_button(self):
        self.driver.instance.back()
        print("\nPressed back button")

    @staticmethod
    def camera(self):
        cmd = "adb shell input keyevent KEYCODE_CAMERA"
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE)
        print("\nADB camera button")

    @staticmethod
    def open_notifications(self):
        self.driver.instance.open_notifications()
        print("\nOpened notifications")

    @staticmethod
    def sleep(num):
        time.sleep(num)

    @staticmethod
    def tap_next(driver):
        TouchAction(driver).tap(None, 950, 1688, 1).perform()



