from features.locators.locators import Locator
from resources import Utilities
from behave import *



class PreventerScreen():

        #def __init__(self, driver):
        #    self.driver = driver
        @given("Android application is opened")
        def step_impl(context):
            pass

        @given("I am on home screen and I tap on medication button")
        def test_navigatetopreventerscreen(context):


            Utilities.mobile_waitForElement(context.driver.instance,
                                            Locator.medication_btn)
            Utilities.mobile_clickElementByID(context.driver.instance,
                                               Locator.medication_btn)

            # Tap back button on medications screen
            Utilities.mobile_waitForElement(context.driver.instance, Locator.back_btn)
            Utilities.click_element_by_id(context.driver.instance, Locator.back_btn)


            # Go back to medications screen
            Utilities.mobile_waitForElement(context.driver.instance,
                                            Locator.medication_btn)
            Utilities.mobile_clickElementByID(context.driver.instance,
                                              Locator.medication_btn)


            Utilities.mobile_waitForElement(context.driver.instance,Locator.logpreventer_btn)
            Utilities.mobile_clickElementByID(context.driver.instance,
                                            Locator.logpreventer_btn)
            self.driver.instance.save_screenshot('screen1.png')


        def test_logpreventerinputs(context):

            Utilities.mobile_waitForElement(context.driver.instance,Locator.preventerheader)


            # Assert default time
            Utilities.assert_text(context.driver.instance,
                                  Locator.time,
                                  Locator.time_txt)


            # Tap on "Now"


        def test_preventertime(self):
            Utilities.click_element_by_name(self.driver.instance,
                                            Locator.time_txt)

            Utilities.mobile_waitForElement(self.driver.instance, Locator.date_time_dialog_header)

            # Assert header text for date time
            Utilities.assert_text(self.driver.instance,
                                  Locator.date_time_dialog_header,
                                  Locator.date_time_dialog_header_txt)



            # Assert header text for done button
            Utilities.assert_text(self.driver.instance,
                                  Locator.done_dialog_header,
                                  Locator.done_dialog_header_txt)


            # Select a date

            # #self.driver.instance.find_element_by_xpath(("//android.widget.NumberPicker[0]/android.widget.EditText[1]")).click()
            # picker = self.driver.instance.find_element_by_xpath("//*[@index='1' and @class='android.widget.NumberPicker']")
            # picker.get(0).sendkeys("07")
            #

            Utilities.click_element_by_name(self.driver.instance, Locator.done_dialog_header_txt)

            # Select medicine

            Utilities.mobile_waitForElement(self.driver.instance,
                                            Locator.selectonemedicine)

            # Assert Select one default text for medicine
            Utilities.assert_text(self.driver.instance, Locator.selectonemedicine,
                                  Locator.medicine)


            # Tap on medicine Select one option
            Utilities.click_element_by_name(self.driver.instance,
                                              Locator.medicine)

            # Tap on medicine name
            Utilities.mobile_waitForElement(self.driver.instance,
                                            Locator.list_popup)
            Utilities.click_element_by_name(self.driver.instance, Locator.medicine_name)

            # Wait for dosage select one
            Utilities.mobile_waitForElement(self.driver.instance,
                                            Locator.selectonedosage)


            # Assert Select one default text for dosage
            Utilities.assert_text(self.driver.instance,
                                  Locator.selectonedosage,
                                  Locator.dosage)

            # Tap on medicine Select one
            Utilities.click_element_by_name(self.driver.instance,
                                            Locator.dosage)

            # Tap on dosage name
            Utilities.mobile_waitForElement(self.driver.instance,
                                            Locator.list_popup)
            Utilities.click_element_by_name(self.driver.instance,
                                            Locator.dosage_name)

            # Check default list of dosage



            # Check state of my go-to preventer
            Utilities.assert_text(self.driver.instance,
                                  Locator.go_to_switch,
                                  Locator.go_to_switch_default_txt)

            # Check default dosage value

            Utilities.assert_text(self.driver.instance, Locator.dosage_default_logpreventer, Locator.dosage_default_logpreventer_txt)


            # Check max dosage value

            Utilities.mobile_waitForElement(self.driver.instance, Locator.dosage_default_logpreventer)

            Utilities.click_element_by_name(self.driver.instance, Locator.dosage_default_logpreventer_txt)

            #Utilities.adb_key(self,'KEYCODE_DPAD_RIGHT')
            #Utilities.adb_key(self,'KEYCODE_FORWARD_DEL')

            #Utilities.press_keycode(self.driver.instance, 22)
            Utilities.press_keycode(self.driver.instance,112)
            Utilities.press_keycode(self.driver.instance, 16)
            Utilities.press_keycode(self.driver.instance, 16)
            Utilities.press_keycode(self.driver.instance, 16)

            self.driver.instance.hide_keyboard()

            #Utilities.enter_text(self.driver.instance,dosage_default_logpreventer,'999' )

            # Tap on log preventer button

        def test_logpreventer(self):

            Utilities.mobile_waitForElement(self.driver.instance,
                                            Locator.logapreventer_btn)
            Utilities.mobile_clickElementByID(self.driver.instance,
                                              Locator.logapreventer_btn)
            self.driver.instance.save_screenshot('screen1.png')

        def test_verifyloggedpreventer(self):

            # Tap on history button
            Utilities.mobile_waitForElement(self.driver.instance,
                                        Locator.history_btn)
            Utilities.mobile_clickElementByID(self.driver.instance,
                                          Locator.history_btn)

            # Check for the "Preventer used" row text
            Utilities.mobile_waitForElement(self.driver.instance,Locator.preventerused)
            Utilities.assert_text(self.driver.instance, Locator.preventerused, Locator.preventerusedtxt_historyscreen)

             # Tap on preventer used
            Utilities.click_element_by_name(self.driver.instance, Locator.preventerusedtxt_historyscreen)


        def test_preventertakendetails(self):


            Utilities.mobile_waitForElement(self.driver.instance, Locator.preventerheader)
            Utilities.assert_text(self.driver.instance, Locator.preventerheader,
                                  Locator.preventerheader_txt)

            # Verify medicine logged
            Utilities.assert_text(self.driver.instance, Locator.medicinepreventerscreen,
                                  Locator.medicinevaluepreventerscreen_txt)

            Utilities.assert_text(self.driver.instance,
                                  Locator.dosagepreventerscreen,
                                  Locator.dosagevaluepreventerscreen_txt)

            Utilities.assert_text(self.driver.instance,
                                  Locator.dosagetype,
                                  Locator.dosagetypepreventerscreen_txt)

            # Verify the time logged

            Utilities.assert_textContains(self.driver.instance, Locator.timestamp, "m")


            # Verify delete log affordance

            Utilities.assert_text(self.driver.instance, Locator.deletepreventer_btn,
                                  Locator.deletepreventer_btn_txt)

            # Tap on delete affordance

            Utilities.click_element_by_name(self.driver.instance,
                                            Locator.deletepreventer_btn_txt)

            # Verify delete dialog box title

            Utilities.mobile_waitForElement(self.driver.instance,
                                            Locator.delete_card_container)

            Utilities.assert_text(self.driver.instance,
                                  Locator.deletedialogtitle,
                                  Locator.deletedialogtitletxt_popup)

            #Utilities.assert_text(self.driver.instance,
            #                      Locator.deletedialogsubtitle,
            #                      Locator.deletedialogsubtitletxt_popup)

            # Tap on cancel button

            #Utilities.click_element_by_name(self.driver.instance,
            #                                Locator.deletedialog_cancelbtn_txt)

            # Verify user is on the preventer taken screen - Debug(code not able to identify the text)


            # Utilities.wait_for_element_by_text(self.driver.instance,
            #                                 Locator.preventerheader_txt)
            # Utilities.assert_text(self.driver.instance,
            #                       Locator.preventerheader,
            #                       Locator.preventerheader_txt)


            # Tap on delete log again

            #Utilities.click_element_by_name(self.driver.instance,
            #                                Locator.deletepreventer_btn_txt)

            #Utilities.mobile_waitForElement(self.driver.instance,
            #                                Locator.delete_card_container)

            Utilities.click_element_by_name(self.driver.instance,
                                            Locator.deletedialog_deletebtn_txt)


            # Go to the home screen - Debug back not working

            #Utilities.mobile_waitForElement(self.driver.instance, Locator.back_btn)
            #Utilities.click_element_by_id(self.driver.instance,Locator.back_btn)





















