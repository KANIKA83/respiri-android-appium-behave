from features.locators.locators import Locator
from resources import Utilities


class PreventerScreenMedicineList():

        def __init__(self, driver):
            self.driver = driver


        def test_medicinelist(self):


            Utilities.mobile_waitForElement(self.driver.instance,
                                            Locator.medication_btn)
            Utilities.mobile_clickElementByID(self.driver.instance,
                                               Locator.medication_btn)

            Utilities.mobile_waitForElement(self.driver.instance,Locator.logpreventer_btn)
            Utilities.mobile_clickElementByID(self.driver.instance,
                                            Locator.logpreventer_btn)
            self.driver.instance.save_screenshot('screen1.png')


            Utilities.mobile_waitForElement(self.driver.instance,Locator.preventerheader)

            Utilities.mobile_waitForElement(self.driver.instance,
                                            Locator.selectonemedicine)
            # Tap on medicine Select one option
            Utilities.click_element_by_name(self.driver.instance,
                                              Locator.medicine)

            # Tap on medicine name
            Utilities.mobile_waitForElement(self.driver.instance,
                                            Locator.list_popup)

            #scroll the list up

            #els = self.driver.instance.find_elements_by_xpath('//android.widget.TextView')
            els = self.driver.instance.find_elements_by_id('co.respiri.weebie.qa:id/item_value')

            for element in els:
                print(element.get_attribute("text"))

            self.driver.instance.scroll(els[4], els[2])

            els = self.driver.instance.find_elements_by_id(
                'co.respiri.weebie.qa:id/item_value')

            for element in els:
                print(element.get_attribute("text"))

            self.driver.instance.scroll(els[5], els[2])
            self.driver.instance.scroll(els[5], els[2])
            self.driver.instance.scroll(els[5], els[2])
            self.driver.instance.scroll(els[5], els[2])
            self.driver.instance.scroll(els[5], els[2])
            self.driver.instance.scroll(els[5], els[2])
            self.driver.instance.scroll(els[5], els[2])
            self.driver.instance.scroll(els[5], els[1])

            Utilities.click_element_by_name(self.driver.instance,
                                            Locator.medicine_name_other)


            #self.driver.instance.scroll(els[len(els) - 1], els[0]) #working
            # Utilities.sleep(3)
            # els = self.driver.instance.find_elements_by_id(
            #     ('co.respiri.weebie.qa:id/item_value'))
            #
            # #print(els[0].get_attribute('text'))
            # print(els[1])
            #Utilities.assert_text(self.driver.instance,
            #                      Locator.medicine_name,
            #                     els[0].text)


