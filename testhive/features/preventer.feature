Feature: Preventer
    As a user
    I should be able to log a preventer

    Scenario: Log preventer when user is connected to internet
        Given I am on home screen and I tap on medication button
        When I get navigated to medication screen
        And I select dosage name and type
        And I tap log preventer button
        Then I am navigated to home screen
        And I see my preventer used on history details screen