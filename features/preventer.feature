@android
    Feature: Preventer tests

  Background: Sign-in and navigate to medication screen
        Given I am on the sign-in screen
        And  I tap on sign-in and enter username and password
        When I am on home screen and I tap on medication button
        Then I get navigated to medication screen
        #Then I am navigated to the welcome screen


    Scenario: Log preventer when user is connected to internet

        When I key in time, select medicine and dosage
        When I log preventer
        Then My preventer appears on history screen
        And  Its same as what I logged
        Then Signout
