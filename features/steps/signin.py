from appium import webdriver
from resources.resources import Utilities
from locators.locators import Locator
from allure_commons._allure import attach
from allure_commons.types import AttachmentType

from behave import *
import sys
import time


sys.path.append('/Users/kanikaagarwal/PycharmProjects/respiri-android-appium-behave/features')

timestr = time.strftime("%Y%m%d-%H%M%S")

@given("I am on the sign-in screen")
def step_impl(context):
    pass


@given("I tap on sign-in and enter username and password")
def entercredentialsforsignin(context):

    Utilities.mobile_waitForElement(context.driver,
                                    Locator.signin_welcomescreen)
    Utilities.mobile_clickElementByID(context.driver,
                                       Locator.signin_welcomescreen)


# Enter credentials

    Utilities.wait_for_element_by_class(context.driver, "android.widget.EditText")

    view = context.driver.current_context

    elements = context.driver.find_elements_by_class_name('android.widget.EditText')

    usernamefield = elements[0]
    passwordfield = elements[1]

    usernamefield.set_value(Locator.username)
    passwordfield.set_value(Locator.password)

    attach(
        context.driver.get_screenshot_as_png(),
        name="Screenshot",
        attachment_type=AttachmentType.PNG
    )

    # Hide keyboard

    context.driver.hide_keyboard()

    #Utilities.wait_for_element_by_text(context.driver,Locator.signin_button_txt)
    Utilities.click_element_by_name(context.driver, Locator.signin_button_txt)


@then("I am navigated to the welcome screen")
def homescreenvalidationpostsignin(context):



    Utilities.mobile_waitForElement(context.driver, Locator.hamburgermenu)
    Utilities.click_element_by_id(context.driver, Locator.hamburgermenu_id)

    attach(
        context.driver.get_screenshot_as_png(),
        name="Screenshot",
        attachment_type=AttachmentType.PNG
    )

    # Signout
    Utilities.wait_for_element_by_text(context.driver, Locator.signout_menu_txt)
    Utilities.click_element_by_name(context.driver, Locator.signout_menu_txt)

    # Tap on confirmation dialog
    Utilities.wait_for_element_by_text(context.driver,Locator.signout_dialog_txt)
    Utilities.click_element_by_name(context.driver,Locator.signout_dialog_txt)

    # Verify user is on welcome screen
    Utilities.mobile_waitForElement(context.driver, Locator.welcometowheezo)
    Utilities.assert_element(context.driver,Locator.welcometowheezo)











