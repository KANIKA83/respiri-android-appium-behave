from appium import webdriver
from resources.resources import Utilities
from locators.locators import Locator
from allure_commons._allure import attach
from allure_commons.types import AttachmentType

from behave import *
import sys


sys.path.append('/Users/kanikaagarwal/PycharmProjects/respiri-android-appium-behave/features')


#@given("Android application is opened")
#def step_impl(context):
#    pass

@when("I am on home screen and I tap on medication button")
def navigatetopreventerscreen(context):

    Utilities.mobile_waitForElement(context.driver,
                                    Locator.medication_btn)
    Utilities.click_element_by_name(context.driver, Locator.medication_btn_txt)
    Utilities.mobile_clickElementByID(context.driver,
                                       Locator.medication_btn)

    # Tap back button on medications screen
    Utilities.mobile_waitForElement(context.driver, Locator.back_btn)
    Utilities.click_element_by_id(context.driver, Locator.back_btn)


    attach(
        context.driver.get_screenshot_as_png(),
        name="Screenshot",
        attachment_type=AttachmentType.PNG
    )


    # Go back to medications screen
    Utilities.mobile_waitForElement(context.driver,
                                    Locator.medication_btn)
    Utilities.mobile_clickElementByID(context.driver,
                                      Locator.medication_btn)

    attach(
        context.driver.get_screenshot_as_png(),
        name="Screenshot",
        attachment_type=AttachmentType.PNG
    )


    Utilities.mobile_waitForElement(context.driver,Locator.logpreventer_btn)
    Utilities.mobile_clickElementByID(context.driver,
                                    Locator.logpreventer_btn)
    #context.driver.save_screenshot('screen1.png')

    attach(
        context.driver.get_screenshot_as_png(),
        name="Screenshot",
        attachment_type=AttachmentType.PNG
    )

@then("I get navigated to medication screen")
def logpreventerinputs(context):
    Utilities.mobile_waitForElement(context.driver,
                                    Locator.preventerheader)
    attach(
        context.driver.get_screenshot_as_png(),
        name="Screenshot",
        attachment_type=AttachmentType.PNG
    )

    # Assert default time
    Utilities.assert_text(context.driver,
                          Locator.time,
                          Locator.time_txt)


    # Tap on "Now"

@when("I key in time, select medicine and dosage")
def preventertime(context):
    Utilities.click_element_by_name(context.driver,
                                    Locator.time_txt)

    Utilities.mobile_waitForElement(context.driver,
                                    Locator.date_time_dialog_header)
    attach(
        context.driver.get_screenshot_as_png(),
        name="Screenshot",
        attachment_type=AttachmentType.PNG
    )

    # Assert header text for date time
    Utilities.assert_text(context.driver,
                          Locator.date_time_dialog_header,
                          Locator.date_time_dialog_header_txt)

    attach(
        context.driver.get_screenshot_as_png(),
        name="Screenshot",
        attachment_type=AttachmentType.PNG
    )

    # Assert header text for done button
    Utilities.assert_text(context.driver,
                          Locator.done_dialog_header,
                          Locator.done_dialog_header_txt)

    # Select a date

    # #self.driver.find_element_by_xpath(("//android.widget.NumberPicker[0]/android.widget.EditText[1]")).click()
    # picker = self.driver.find_element_by_xpath("//*[@index='1' and @class='android.widget.NumberPicker']")
    # picker.get(0).sendkeys("07")
    #

    Utilities.click_element_by_name(context.driver,
                                    Locator.done_dialog_header_txt)

    # Select medicine

    Utilities.mobile_waitForElement(context.driver,
                                    Locator.selectonemedicine)

    attach(
        context.driver.get_screenshot_as_png(),
        name="Screenshot",
        attachment_type=AttachmentType.PNG
    )

    # Assert Select one default text for medicine
    Utilities.assert_text(context.driver, Locator.selectonemedicine,
                          Locator.medicine)

    # Tap on medicine Select one option
    Utilities.click_element_by_name(context.driver,
                                    Locator.medicine)

    # Tap on medicine name
    Utilities.mobile_waitForElement(context.driver,
                                    Locator.list_popup)
    Utilities.click_element_by_name(context.driver,
                                    Locator.medicine_name)

    # Wait for dosage select one
    Utilities.mobile_waitForElement(context.driver,
                                    Locator.selectonedosage)

    attach(
        context.driver.get_screenshot_as_png(),
        name="Screenshot",
        attachment_type=AttachmentType.PNG
    )

    # Assert Select one default text for dosage
    Utilities.assert_text(context.driver,
                          Locator.selectonedosage,
                          Locator.dosage)

    # Tap on medicine Select one
    Utilities.click_element_by_name(context.driver,
                                    Locator.dosage)

    # Tap on dosage name
    Utilities.mobile_waitForElement(context.driver,
                                    Locator.list_popup)
    Utilities.click_element_by_name(context.driver,
                                    Locator.dosage_name)

    attach(
        context.driver.get_screenshot_as_png(),
        name="Screenshot",
        attachment_type=AttachmentType.PNG
    )

    # Check default list of dosage

    # Check state of my go-to preventer
    Utilities.assert_text(context.driver,
                          Locator.go_to_switch,
                          Locator.go_to_switch_default_txt)

    attach(
        context.driver.get_screenshot_as_png(),
        name="Screenshot",
        attachment_type=AttachmentType.PNG
    )

    # Check default dosage value

    Utilities.assert_text(context.driver,
                          Locator.dosage_default_logpreventer,
                          Locator.dosage_default_logpreventer_txt)

    # Check max dosage value

    Utilities.mobile_waitForElement(context.driver,
                                    Locator.dosage_default_logpreventer)

    Utilities.click_element_by_name(context.driver,
                                    Locator.dosage_default_logpreventer_txt)

    # Utilities.adb_key(self,'KEYCODE_DPAD_RIGHT')
    # Utilities.adb_key(self,'KEYCODE_FORWARD_DEL')

    # Utilities.press_keycode(self.driver, 22)
    Utilities.press_keycode(context.driver, 112)
    Utilities.press_keycode(context.driver, 16)
    Utilities.press_keycode(context.driver, 16)
    Utilities.press_keycode(context.driver, 16)

    context.driver.hide_keyboard()

    attach(
        context.driver.get_screenshot_as_png(),
        name="Screenshot",
        attachment_type=AttachmentType.PNG
    )

    # Utilities.enter_text(self.driver,dosage_default_logpreventer,'999' )

    # Tap on log preventer button

@when("I log preventer")
def logpreventer(context):
    Utilities.mobile_waitForElement(context.driver,
                                    Locator.logapreventer_btn)
    Utilities.mobile_clickElementByID(context.driver,
                                      Locator.logapreventer_btn)



@then("My preventer appears on history screen")
def verifyloggedpreventer(context):
    # Tap on history button
    Utilities.mobile_waitForElement(context.driver,
                                    Locator.history_btn)
    Utilities.mobile_clickElementByID(context.driver,
                                      Locator.history_btn)

    # Check for the "Preventer used" row text
    Utilities.mobile_waitForElement(context.driver,
                                    Locator.preventerused)

    attach(
        context.driver.get_screenshot_as_png(),
        name="Screenshot",
        attachment_type=AttachmentType.PNG
    )

    Utilities.assert_text(context.driver, Locator.preventerused,
                          Locator.preventerusedtxt_historyscreen)

    # Tap on preventer used
    Utilities.click_element_by_name(context.driver,
                                    Locator.preventerusedtxt_historyscreen)

@then("Its same as what I logged")
def preventertakendetails(context):
    Utilities.mobile_waitForElement(context.driver,
                                    Locator.preventerheader)

    attach(
        context.driver.get_screenshot_as_png(),
        name="Screenshot",
        attachment_type=AttachmentType.PNG
    )

    Utilities.assert_text(context.driver, Locator.preventerheader,
                          Locator.preventerheader_txt)

    # Verify medicine logged
    Utilities.assert_text(context.driver,
                          Locator.medicinepreventerscreen,
                          Locator.medicinevaluepreventerscreen_txt)

    Utilities.assert_text(context.driver,
                          Locator.dosagepreventerscreen,
                          Locator.dosagevaluepreventerscreen_txt)

    Utilities.assert_text(context.driver,
                          Locator.dosagetype,
                          Locator.dosagetypepreventerscreen_txt)

    # Verify the time logged

    Utilities.assert_textContains(context.driver, Locator.timestamp, "m")

    # Verify delete log affordance

    Utilities.assert_text(context.driver, Locator.deletepreventer_btn,
                          Locator.deletepreventer_btn_txt)

    # Tap on delete affordance

    Utilities.click_element_by_name(context.driver,
                                    Locator.deletepreventer_btn_txt)

    # Verify delete dialog box title

    Utilities.mobile_waitForElement(context.driver ,
                                    Locator.delete_card_container)

    Utilities.assert_text(context.driver ,
                          Locator.deletedialogtitle,
                          Locator.deletedialogtitletxt_popup)

    # Utilities.assert_text(self.driver ,
    #                      Locator.deletedialogsubtitle,
    #                      Locator.deletedialogsubtitletxt_popup)

    # Tap on cancel button

    #Utilities.click_element_by_name(context.driver ,
    #                                Locator.deletedialog_cancelbtn_txt)

    # Verify user is on the preventer taken screen - Debug(code not able to identify the text)

    # Utilities.wait_for_element_by_text(self.driver ,
    #                                 Locator.preventerheader_txt)
    # Utilities.assert_text(self.driver ,
    #                       Locator.preventerheader,
    #                       Locator.preventerheader_txt)

    # Tap on delete log again

    #Utilities.click_element_by_name(context.driver ,
    #                                Locator.deletepreventer_btn_txt)

    # Utilities.mobile_waitForElement(self.driver ,
    #                                Locator.delete_card_container)

    Utilities.click_element_by_name(context.driver ,
                                    Locator.deletedialog_deletebtn_txt)

    attach(
        context.driver.get_screenshot_as_png(),
        name="Screenshot",
        attachment_type=AttachmentType.PNG
    )



    # Go to the home screen

    Utilities.press_keycode(context.driver,4)
    #Utilities.sleep(2)



   # attach(
   #     context.driver.get_screenshot_as_png(),
   #     name="Screenshot",
   #     attachment_type=AttachmentType.PNG
   # )

    # Tap on menu to sign out

@then("Signout")
def signout(context):

    #print(context.driver.page_source)

    Utilities.sleep(15)

    Utilities.mobile_waitForElement(context.driver, Locator.hamburgermenu)
    Utilities.click_element_by_id(context.driver, Locator.hamburgermenu_id)


    Utilities.wait_for_element_by_text(context.driver,
                                       Locator.signout_menu_txt)
    Utilities.click_element_by_name(context.driver, Locator.signout_menu_txt)

    # Tap on confirmation dialog
    Utilities.wait_for_element_by_text(context.driver,
                                       Locator.signout_dialog_txt)
    Utilities.click_element_by_name(context.driver, Locator.signout_dialog_txt)


if __name__ == '__main__':
    context.driver = webdriver.Remote()