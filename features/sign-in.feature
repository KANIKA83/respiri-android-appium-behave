@android
    Feature: Sign-in tests
     @current
    Scenario: Sign-in flow when phone is connected to internet
        Given I am on the sign-in screen
        And  I tap on sign-in and enter username and password
        Then I am navigated to the welcome screen
